package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.example.demo.config.SpringBootH2Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DemoApplication.class, SpringBootH2Application.class}, properties = { "spring.jpa.database-platform=org.hibernate.dialect.H2Dialect" })
@Sql("/init.sql")
public class CrearSucursalTests {
    private final AtomicLong counter = new AtomicLong();
    RestTemplate restTemplate = new RestTemplate();
    private final Logger log = LoggerFactory.getLogger(CrearSucursalTests.class);
    
    @Test
    public void creacionExitosa() {        
        HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept((Collections.singletonList(MediaType.APPLICATION_JSON)));
        Sucursal sucursal = new Sucursal(counter.incrementAndGet(), "-31.427391274141634", "-64.18940918730925", "José manuel Estarada 160", "9 a 18 hs");
        HttpEntity<Sucursal> requestEntity = new HttpEntity<>(sucursal, requestHeaders);
        ResponseEntity<Sucursal> response = restTemplate.postForEntity( "http://localhost:8080/sucursal/", requestEntity, Sucursal.class );
        assertEquals("-31.427391274141634", response.getBody().getLatitud());
    }

    /**
     * Va fallar porque se valida que los datos no sean nulos o vacios
     */
    @Test
    public void creacionFallida() {
    	HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept((Collections.singletonList(MediaType.APPLICATION_JSON)));
        Sucursal sucursal = new Sucursal();
        HttpEntity<Sucursal> requestEntity = new HttpEntity<>(sucursal, requestHeaders);
        ResponseEntity<Sucursal> response = restTemplate.postForEntity( "http://localhost:8080/sucursal/", requestEntity, Sucursal.class );
        assertEquals("-31.427391274141634", response.getBody().getLatitud());
    }

    
}