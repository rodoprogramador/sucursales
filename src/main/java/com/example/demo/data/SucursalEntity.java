package com.example.demo.data;

import javax.persistence.*;

@Entity
@Table(name="Sucursal")
public class SucursalEntity extends NodoEntity {
    
	private String direccion;
	private String horarioAtencion;
    
//    @Id
//    @Column(name = "ID")
//    public long getId() {
//        return id;
//    }

//    public void setId(long id){
//        this.id = id;
//    }
    
    public String getDireccion(){
        return direccion;
    }

    public String getHorarioAtencion(){
        return horarioAtencion;
    }

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public void setHorarioAtencion(String horarioAtencion) {
		this.horarioAtencion = horarioAtencion;
	}
}