package com.example.demo.data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@MappedSuperclass
@Table(name="Nodo")
public class NodoEntity {
	private long id;
	private String latitud;
	private String longitud;	
	
	  @Id
	  @Column(name = "ID")
	  public long getId() {
	      return id;
	  }
	
	  public void setId(long id){
	      this.id = id;
	  }

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	  
}
