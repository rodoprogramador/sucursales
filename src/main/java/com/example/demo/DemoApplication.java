package com.example.demo;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import com.example.demo.service.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@Autowired
	NodoServiceImpl nodoService;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s!", name);
	}

	/**
	 * Creación
	 * @param s
	 * @return
	 */
    @PostMapping("/sucursal")
    ResponseEntity<Sucursal> altaSucursal(@RequestBody @Valid Sucursal s){
        return nodoService.alta(s);
    }
	
    /**
	 * Edición
	 * @param s
	 * @return
	 */
    @PostMapping("/editarSucursal")
    ResponseEntity<Sucursal> edicionSucursal(@RequestBody @Valid Sucursal s){
    	return nodoService.modificacion(s);
    }
    
    /**
	 * Borrado
	 * @param s
	 * @return
	 */
    @DeleteMapping(value = "/sucursal/{id}")
    ResponseEntity<Sucursal> edicionSucursal(@PathVariable Long id){
    	return nodoService.borrar(id);
    }
    
    @GetMapping("/sucursal/{id}")
    public Set<Sucursal> obtenerPorId(@PathVariable Long id){

        return nodoService.obtenerPorId(id);
    }
}
