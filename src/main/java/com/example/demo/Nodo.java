package com.example.demo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Nodo {
	
	@NotNull
    private long id;
	@NotNull
	@NotEmpty
    private String latitud;
	@NotNull
	@NotEmpty
    private String longitud;

    public Nodo() {
    	
    }
    
    public Nodo(long id, String latitud, String longitud){
        this.id = id;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public void setId(long id) {
		this.id = id;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public long getId(){
        return id;
    }

    public String getLatitud(){
        return latitud;
    }

    public String getLongitud(){
        return longitud;
    }
}