package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.data.SucursalEntity;
@Repository
public interface SucursalRepository extends JpaRepository<SucursalEntity, Long>{
    
}
