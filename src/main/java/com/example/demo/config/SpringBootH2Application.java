package com.example.demo.config;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.jpa.HibernatePersistenceProvider;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.example.demo.Repository", "com.example.demo.data" })
public class SpringBootH2Application {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootH2Application.class, args);
    }

    @Bean(name = "dataSource")
    public DataSource dataSource() throws NamingException{
        return DataSourceBuilder.create().driverClassName("org.h2.Driver").url("jdbc:h2:mem:h2console").username("sa").password("").build();
    }
    
    @Bean()
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException {
      LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
      em.setPackagesToScan("com.example.demo.data");
      em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
      em.setDataSource(dataSource());
      return em;
    }

    @Bean
    public PlatformTransactionManager transactionManager( EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    
}
