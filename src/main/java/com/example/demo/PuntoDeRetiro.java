package com.example.demo;

public class PuntoDeRetiro extends Nodo {
    public final long capacidad;

    public PuntoDeRetiro(long id, String latitud, String longitud, long capacidad){
        super(id, latitud, longitud);
        this.capacidad = capacidad;
    }

    public long getCapacidad(){
        return capacidad;
    }

}