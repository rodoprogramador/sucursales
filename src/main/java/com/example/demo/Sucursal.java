package com.example.demo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Sucursal extends Nodo {
	@NotNull
	@NotEmpty
    private String direccion;
	@NotNull
	@NotEmpty
    private String horarioAtencion;

    public Sucursal(long id, String latitud, String longitud, String direccion, String horarioAtencion){
        super(id, latitud, longitud);
        this. direccion = direccion;
        this.horarioAtencion = horarioAtencion;
    }
    
    public Sucursal() {
    	super();
    }

	public String getDireccion(){
        return direccion;
    }

    public String getHorarioAtencion(){
        return horarioAtencion;
    }

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public void setHorarioAtencion(String horarioAtencion) {
		this.horarioAtencion = horarioAtencion;
	}
}