package com.example.demo.service;

import com.example.demo.Sucursal;
import com.example.demo.data.SucursalEntity;
import com.example.demo.repository.SucursalRepository;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class NodoServiceImpl {
	@Autowired
    SucursalRepository sucursalRepo;

    public ResponseEntity<Sucursal> alta(Sucursal s) {
    	
    	SucursalEntity sucursal = new SucursalEntity();
    	sucursal.setId(s.getId());
    	sucursal.setLatitud(s.getLatitud());
    	sucursal.setLongitud(s.getLongitud());
    	sucursal.setDireccion(s.getDireccion());
    	sucursal.setHorarioAtencion(s.getHorarioAtencion());
    	sucursalRepo.save(sucursal);
		ResponseEntity<Sucursal> response = new ResponseEntity<Sucursal>(s, HttpStatus.OK);
		return response ;
    }

	public ResponseEntity<Sucursal> modificacion(@Valid Sucursal s) {
		// TODO Auto-generated method stub
		return null;
	}

	public ResponseEntity<Sucursal> borrar(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Sucursal> obtenerPorId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
